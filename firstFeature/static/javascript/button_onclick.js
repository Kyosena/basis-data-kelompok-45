var clicked="admin";
function reset() {
	hideDokter();
	hidePasien();
	$("body").css("height", "150vh");
	$(".rectangle").toggle();
	$(".register").toggle();
	$(".logo").toggle();
	$(".rectangle").css("height", "115vh");
	$(".register").css("top", "110vh");
	$(".rectangle").toggle(700);
	$(".register").toggle(700);
	$(".logo").toggle(700);
}

function hideDokter() {
	if(clicked == "dokter"){
		$(".nomor_SIP").toggle(700);
		$("#id_nomor_SIP").toggle(700);
		$(".spesialisasi").toggle(700);
		$("#id_spesialisasi").toggle(700);
	}
}

function hidePasien() {
	if(clicked == "pasien"){
		$(".alergi").toggle(700);
		$("#id_alergi").toggle(700);
	}
}

$(document).ready(function() {
	
  $(".dokter").click(function() {
	if(clicked != "dokter"){
		$("#id_role").val("dokter");
		hidePasien();
		$(".dokter").css("background-color","#FF7364");
		$(".admin").css("background-color","#3750B2");
		$(".pasien").css("background-color","#3750B2");
		$("#id_alergi").css("display","none");
		$("body").css("height", "160vh");
		$(".rectangle").toggle();
		$(".register").toggle();
		$(".logo").toggle();
		$(".rectangle").css("height", "130vh");
		$(".register").css("top", "125.5vh");
		$(".rectangle").toggle(700);
		$(".register").toggle(700);
		$(".nomor_SIP").toggle(700);
		$("#id_nomor_SIP").toggle(700);
		$(".spesialisasi").toggle(700);
		$("#id_spesialisasi").toggle(700);
		$(".logo").toggle(700);
		clicked = "dokter";
	}
  });
  
  $(".admin").click(function() {
	if(clicked != "admin"){
		$("#id_role").val("admin");
		reset();
		clicked = "admin";
		$(".dokter").css("background-color","#3750B2");
		$(".admin").css("background-color","#FF7364");
		$(".pasien").css("background-color","#3750B2");
	}
  });
  
  $(".pasien").click(function() {
	if(clicked != "pasien"){
		$("#id_role").val("pasien");
		hideDokter();
		$(".dokter").css("background-color","#3750B2");
		$(".admin").css("background-color","#3750B2");
		$(".pasien").css("background-color","#FF7364");
		$("body").css("height", "155vh");
		$(".rectangle").toggle();
		$(".register").toggle();
		$(".logo").toggle();
		$(".rectangle").css("height", "120vh");
		$(".register").css("top", "117vh");
		$(".rectangle").toggle(700);
		$(".register").toggle(700);
		$(".alergi").toggle(700);
		$("#id_alergi").toggle(700);
		$(".logo").toggle(700);
		clicked = "pasien";
	}
  });
  
  $(".register").click(function() {
	  if($("#id_spesialisasi").val() == ""){
		  $("#id_spesialisasi").val("-");
	  }if($("#id_nomor_SIP").val() == ""){
		  $("#id_nomor_SIP").val("-");
	  }if($("#id_alergi").val() == ""){
		  $("#id_alergi").val("-");
	  }
  });
});