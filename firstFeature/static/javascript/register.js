var x = 0;
var clicked="admin";
var penggunaOk = true;
var dokterOk = true;
var pasienOk = true;
function reset() {
	hideDokter();
	hidePasien();
	$(".button").toggle();
	$(".button").toggle(700);
	resetBox();
}

function hideDokter() {
	if(clicked == "dokter"){
		$("#nomor_SIP").toggle(700);
		$("#spesialisasi").toggle(700);
	}
}

function checkPenggunaOk() {
	penggunaOk = true;
	$(".pengguna_form").each(function(idx, elem) {
		if($(elem).val() == ""){
		  penggunaOk = false;
		}
	});
}

function checkDokterOk() {
	dokterOk = true;
	$(".dokter_form").each(function(idx, elem) {
		if($(elem).val() == "" || penggunaOk == false){
		  dokterOk = false;
		}
	});
}

function checkPasienOk() {
	pasienOk = true;
	$(".pasien_form").each(function(idx, elem) {
		if($(elem).val() == "" || penggunaOk == false){
		  pasienOk = false;
		}
	});
}

function runSubmit() {
	checkPenggunaOk();
	checkDokterOk();
	checkPasienOk();
	if(clicked == "dokter" && dokterOk == true){
		$(".pasien_form").each(function(idx, elem) {
			  if($(elem).val() == ""){
				  $(elem).val("-");
			  }
		});
	}else if(clicked == "pasien" && pasienOk == true){
		$(".dokter_form").each(function(idx, elem) {
			  if($(elem).val() == ""){
				  $(elem).val("-");
			  }
		});
	}else if(clicked == "admin" && penggunaOk == true){
		$(".pasien_form").each(function(idx, elem) {
			  if($(elem).val() == ""){
				  $(elem).val("-");
			  }
		});
		$(".dokter_form").each(function(idx, elem) {
			  if($(elem).val() == ""){
				  $(elem).val("-");
			  }
		});
	}
}

function displayButton() {
	if($(".button").css("display") == "none"){
		$(".button").toggle(700);
	}
}

function hidePasien() {
	if(clicked == "pasien"){
		$("#id_tindakan").toggle(700);
	}
}

function addBody() {
	var x = $("#body_buat_tindakan").css("height");
	var s = x.length;
	var y = x.substring(0,s-2);
	var z = parseInt(y);
	z+=50;
	$("#body_buat_tindakan").css("height",z+"px");
}

function resetBox(){
	var x = $("#box").height();
	x += 50;
	$("body").css("height",x+"px");
}
function substractBody() {
	var x = $("#body_buat_tindakan").css("height");
	var s = x.length;
	var y = x.substring(0,s-2);
	var z = parseInt(y);
	z-=50;
	$("#body_buat_tindakan").css("height",z+"px");
}

function checkDuplicate() {
	var types = [];
	var typesFiltered = [];
	$("select[name='id_tindakan_form'] :selected").each(function() {
       types.push($(this).val());
	   if(jQuery.inArray($(this).val(), typesFiltered) == -1){
		   typesFiltered.push($(this).val());
	   }
    });
	if(types.length != typesFiltered.length){
		return true;
	}
}

(function() {
  'use strict';
  window.addEventListener('load', function() {
	resetBox();
	var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
		runSubmit();
		if(checkDuplicate()){
			event.preventDefault();
			alert("Terdapat Duplikasi di Id Transaksi");
			event.stopImmediatePropagation();
		}
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
		  $("#body_buat_tindakan").css("height","870px")
		  form.classList.add('was-validated');
        }
      }, false);
    });
  }, false);
  
  $("#id_tindakan").on('click', "#button_plus_active", function() {
	  x += 1;
	  addBody();
	  $("#button_plus_active").attr("id","button_plus_passive");
	  $("#button_minus_active").attr("id","button_minus_passive");
	  $("#button_plus_passive").remove();
	  $("#button_minus_passive").remove();
	  $("#id_tindakan").append("<div class='form-group col-md-8'><input type='text' name='alergi' class='form-control form-control-sm input pasien_form' required><div class='valid-feedback'>Looks good!</div><div class='invalid-feedback'>Please enter a value. Or check if there is any duplications</div></div><div class='form-group col-md-2'><button type='button' class='button_plus' id='button_plus_active' style='margin-top:0px;'>+</button></div><div class='form-group col-md-2'><button type='button' class='button_plus' id='button_minus_active' style='margin-top:0px;'>-</button></div>");
  });
  
  $("#id_tindakan").on('click', "#button_minus_active", function() {
	  substractBody();
	  if(x==1){
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").append("<div class='form-group col-md-2'><button type='button' class='button_plus' id='button_plus_active'>+</button></div>");
	  }else{
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").find("div:last-child").remove();
		  $("#id_tindakan").append("<div class='form-group col-md-2'><button type='button' class='button_plus' id='button_plus_active' style='margin-top:0px;'>+</button></div><div class='form-group col-md-2'><button type='button' class='button_plus' id='button_minus_active' style='margin-top:0px;'>-</button></div>");
	  }
	  x -= 1;
	});
	
	$("#dokter").click(function() {
	if(clicked != "dokter"){
		hidePasien();
		$("#dokter").attr('class','button_clicked');
		$("#admin").attr('class','button_unclicked');
		$("#pasien").attr('class','button_unclicked');
		$(".button").toggle();
		$(".button").toggle(700);
		$("#nomor_SIP").toggle(700);
		$("#spesialisasi").toggle(700);
		displayButton()
		resetBox();
		clicked = "dokter";
		$("#role_clicked").val(clicked);
	}
  });
  
  $("#admin").click(function() {
	if(clicked != "admin"){
		reset();
		clicked = "admin";
		$("#dokter").attr('class','button_unclicked');
		$("#admin").attr('class','button_clicked');
		$("#pasien").attr('class','button_unclicked');
		$("#role_clicked").val(clicked);
		displayButton()
	}
  });
  
  $("#pasien").click(function() {
	if(clicked != "pasien"){
		hideDokter();
		$("#dokter").attr('class','button_unclicked');
		$("#admin").attr('class','button_unclicked');
		$("#pasien").attr('class','button_clicked');
		$(".button").toggle();
		$(".button").toggle(700);
		$("#id_tindakan").toggle(700);
		resetBox();
		clicked = "pasien";
		$("#role_clicked").val(clicked);
		displayButton()
	}
  });
})();