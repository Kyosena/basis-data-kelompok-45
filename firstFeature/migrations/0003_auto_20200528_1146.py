# Generated by Django 2.2.6 on 2020-05-28 04:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('firstFeature', '0002_auto_20200503_1711'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Admin',
        ),
        migrations.DeleteModel(
            name='Dokter',
        ),
        migrations.DeleteModel(
            name='Pasien',
        ),
        migrations.DeleteModel(
            name='Pengguna',
        ),
    ]
