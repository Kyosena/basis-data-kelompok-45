from django import forms
from django.forms.formsets import formset_factory
from django.forms import widgets

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}), max_length=50)
    
class RegisterForm(forms.Form):
    role = forms.CharField(max_length=50, initial="admin")
    username = forms.CharField(max_length=50)
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}))
    nomor_identitas = forms.CharField(max_length=50)
    nama_lengkap = forms.CharField(max_length=50)
    tanggal_lahir = forms.DateField(widget=forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),input_formats=('%Y-%m-%d', ))
    email = forms.EmailField(widget=forms.EmailInput(attrs={"class": "right"}))
    alamat = forms.CharField(widget=forms.Textarea)
    nomor_SIP = forms.CharField(max_length=50)
    spesialisasi = forms.CharField(widget=forms.Textarea)
    alergi = forms.CharField(max_length=50)
    
class AlergiForm(forms.Form):
    alergi = forms.CharField(label="alergi",max_length=50)
    
AlergiFormset = formset_factory(AlergiForm, extra=1)