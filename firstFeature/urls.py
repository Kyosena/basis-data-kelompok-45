from django.urls import path
from . import views

app_name = 'firstFeature'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('login/', views.loginPage, name='login_page'),
    path('register/', views.registerPage, name='register_page'),
    path('logout/', views.logout, name='logout'),
]