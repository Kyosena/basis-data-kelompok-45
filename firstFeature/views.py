from django.db import connection, DatabaseError, transaction
from django.shortcuts import render, redirect
from .forms import LoginForm, RegisterForm, AlergiFormset

def homepage(request):
    if 'Username' in request.session:
        return redirect("greenFeature:profile_page")
    return render(request, 'homepage.html')
    
def loginPage(request):
    if 'Username' in request.session:
        return redirect("greenFeature:profile_page")
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            if verifyLoginValidation(username, password):
                role = getPenggunaType(username)
                request.session['Role'] = role
                request.session['Username'] = username
                return redirect('greenFeature:profile_page')
            else:
                return render(request, 'login.html', {'form':form, 'err':'data yang dimasukkan tidak terdata'})
    else :
        form = LoginForm()
    return render(request, 'login.html', {'form':form})

def registerPage(request):
    if 'Username' in request.session:
        return redirect("greenFeature:profile_page")
    if request.method == "POST":
        form = RegisterForm(request.POST)
        role = request.POST['role']
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nama_lengkap = request.POST['nama_lengkap']
        tanggal_lahir = request.POST['tanggal_lahir']
        email = request.POST['email']
        alamat = request.POST['alamat']
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO pengguna VALUES (%s, %s, %s, %s, %s, %s, %s)',[email, username, password, nama_lengkap, nomor_identitas, tanggal_lahir, alamat])
            saveRole(role, request, username)
            request.session['Role'] = role
            request.session['Username'] = username
            return redirect('greenFeature:profile_page')
        except DatabaseError as e:
            return render(request, 'register.html', {'form':form, 'err':'username, nomor_identitas, atau email sudah terdaftar'})
    else :
        form = RegisterForm()
    return render(request, 'register.html', {'form':form})

def logout(request):
    request.session.flush()
    return redirect("firstFeature:homepage")

def saveRole(role, request, username):
    with transaction.atomic():
        with connection.cursor() as cursor:
            if role == "admin":
                pk = generateNomorPegawai()
                cursor.execute('INSERT INTO administrator VALUES (%s, %s, %s)',[pk, username, "RS1"])
            elif role == "dokter":
                pk = generateIdDokter()
                nomor_SIP = request.POST['nomor_SIP']
                spesialisasi = request.POST['spesialisasi']
                cursor.execute('INSERT INTO dokter VALUES (%s, %s, %s, %s)',[pk, username, nomor_SIP, spesialisasi])
            else:
                pk = generateNRM()
                alergiList = request.POST.getlist('alergi')
                cursor.execute('INSERT INTO pasien VALUES (%s, %s, %s)',[pk, username, "Hanwha Life Indonesia"])
                for alergi in alergiList:
                    cursor.execute('INSERT INTO alergi_pasien VALUES (%s, %s)',[pk, alergi])
        
def verifyLoginValidation(username, password):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM pengguna WHERE username = %s AND password = %s', [username, password])
        pengguna = cursor.fetchone()
        if (pengguna):
            return True
        return False

def getPenggunaType(username):
    with connection.cursor() as cursor:
        cursor.execute('SELECT username FROM dokter WHERE username = %s', [username])
        dokter = cursor.fetchone()
        if (dokter):
            return "dokter"
        cursor.execute('SELECT username FROM pasien WHERE username = %s', [username])
        pasien = cursor.fetchone()
        if (pasien):
            return "pasien"
        return "admin"
            
def generateNRM():
    with connection.cursor() as cursor:
        cursor.execute('SELECT no_rekam_medis FROM pasien ORDER BY RIGHT(no_rekam_medis, -3)::int DESC LIMIT 1')
        pasien = cursor.fetchone()
        if(pasien):
            nrm = pasien[0]
            new_number = int(nrm[3::])+1
            return "NRM"+str(new_number)
        return "NRM1"
        
def generateIdDokter():
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_dokter FROM dokter ORDER BY RIGHT(id_dokter, -2)::int DESC LIMIT 1')
        dokter = cursor.fetchone()
        if(dokter):
            id_dokter = dokter[0]
            new_number = int(id_dokter[2::])+1
            return "DK"+str(new_number)
        return "DK1"
        
def generateNomorPegawai():
    with connection.cursor() as cursor:
        cursor.execute('SELECT nomor_pegawai FROM administrator ORDER BY RIGHT(nomor_pegawai, -2)::int DESC LIMIT 1')
        admin = cursor.fetchone()
        if(admin):
            nomor_pegawai = admin[0]
            new_number = int(nomor_pegawai[2::])+1
            return "AD"+str(new_number)
        return "AD1"