$(document).ready(function() {
	var y = $("table").height();
	y += 18;
	$("#box").css("height",y+"px");
	var x = $(document).height();
	x += 30;
	$("body").css("height",x+"px");
	
	$(".button_delete").click(function() {
		resetBoxHeight();
		$(this).parent().parent().remove();
	});
});

function resetBoxHeight(){
	var y = $("#box").height();
	y -= 40;
	$("#box").css("height",y+"px");
	var x = $(document).height();
	x -= 40;
	$("body").css("height",x+"px");
}