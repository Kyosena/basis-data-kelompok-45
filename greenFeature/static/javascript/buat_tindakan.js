var x = 0;
(function() {
  'use strict';
  window.addEventListener('load', function() {
	var x = $(document).height();
	x += 30;
	$("body").css("height",x+"px");
	var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
		if(checkDuplicate()){
			event.preventDefault();
			alert("Terdapat Duplikasi di Id Transaksi");
			event.stopImmediatePropagation();
		}
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
		  $("#body_buat_tindakan").css("height","870px")
		  form.classList.add('was-validated');
        }
      }, false);
    });
  }, false);
  
  $("#id_tindakan").on('click', "#button_plus_active", function() {
	  x += 1;
	  addBody();
	  $("#button_plus_active").attr("id","button_plus_passive");
	  $("#button_minus_active").attr("id","button_minus_passive");
	  $("#button_plus_passive").remove();
	  $("#button_minus_passive").remove();
	  $(".tindakan_poli_dropdown").last().clone().appendTo(".form-row");
	  $(".tindakan_poli_dropdown").last().removeClass("col-md-10");
	  if(x==1){$("label").last().remove();}
	  $(".tindakan_poli_dropdown").last().addClass("col-md-8")
	  $(".form-row").append("<div class='form-group col-md-2'><button type='button' class='button_plus' id='button_plus_active' style='margin-top:0px;'>+</button></div><div class='form-group col-md-2'><button type='button' class='button_plus' id='button_minus_active' style='margin-top:0px;'>-</button></div>");
  });
  
  $("#id_tindakan").on('click', "#button_minus_active", function() {
	  substractBody();
	  if(x==1){
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").append("<div class='form-group col-md-2'><button type='button' class='button_plus' id='button_plus_active'>+</button></div>");
	  }else{
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").find("div:last-child").remove();
		  $(".form-row").append("<div class='form-group col-md-2'><button type='button' class='button_plus' id='button_plus_active' style='margin-top:0px;'>+</button></div><div class='form-group col-md-2'><button type='button' class='button_plus' id='button_minus_active' style='margin-top:0px;'>-</button></div>");
	  }
	  x -= 1;
	});
})();

function addBody() {
	var x = $("#body_buat_tindakan").css("height");
	var s = x.length;
	var y = x.substring(0,s-2);
	var z = parseInt(y);
	z+=47;
	$("#body_buat_tindakan").css("height",z+"px");
}

function substractBody() {
	var x = $("#body_buat_tindakan").css("height");
	var s = x.length;
	var y = x.substring(0,s-2);
	var z = parseInt(y);
	z-=47;
	$("#body_buat_tindakan").css("height",z+"px");
}

function checkDuplicate() {
	var types = [];
	var typesFiltered = [];
	$("select[name='id_tindakan_form'] :selected").each(function() {
       types.push($(this).val());
	   if(jQuery.inArray($(this).val(), typesFiltered) == -1){
		   typesFiltered.push($(this).val());
	   }
    });
	if(types.length != typesFiltered.length){
		return true;
	}
}