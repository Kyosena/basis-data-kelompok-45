from django.db import connection, DatabaseError, transaction
from django.shortcuts import render, redirect, reverse

def profilePage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        alergi = ""
        cursor.execute('SELECT * FROM pengguna WHERE username = %s',[username])
        user = cursor.fetchone()
        user_role = getUserBasedOnRole(role, username)
        if(role == "pasien"):
            cursor.execute("SELECT pasien.no_rekam_medis, STRING_AGG(alergi_pasien.alergi,', ') FROM pasien INNER JOIN alergi_pasien ON pasien.no_rekam_medis = alergi_pasien.no_rekam_medis WHERE pasien.no_rekam_medis = %s GROUP BY pasien.no_rekam_medis", [user_role[0]])
            alergi_pasien = cursor.fetchone()
            alergi = alergi_pasien[1]
    return render(request, 'profile_'+role+".html", {'role':role,'user':user,'user_role':user_role,'alergi':alergi})
    
def buatTindakanPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_konsultasi FROM sesi_konsultasi ORDER BY RIGHT(id_konsultasi, -2)::int')
        id_konsultasis = cursor.fetchall()
        cursor.execute('SELECT id_transaksi FROM transaksi ORDER BY RIGHT(id_transaksi, -2)::int')
        id_transaksis = cursor.fetchall()
        cursor.execute('SELECT id_tindakan_poli FROM tindakan_poli ORDER BY RIGHT(id_tindakan_poli, -2)::int')
        id_tindakan_polis = cursor.fetchall()
    if request.method == 'POST':
        id_konsultasi = request.POST["id_konsultasi"]
        id_transaksi = request.POST["id_transaksi"]
        catatan = request.POST["catatan"]
        id_tindakan_list = request.POST.getlist("id_tindakan_form")
        nomor_urut = generateNomorUrut(id_konsultasi)
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO tindakan VALUES (%s, %s, %s, %s, %s)', [id_konsultasi, nomor_urut, 0, catatan, id_transaksi])
                    for id_tindakan_poli in id_tindakan_list:
                        cursor.execute('INSERT INTO daftar_tindakan VALUES (%s, %s, %s)', [id_konsultasi, nomor_urut, id_tindakan_poli])
            return redirect('greenFeature:daftar_tindakan')
        except DatabaseError as e:
            return render(request, "buat_tindakan.html", {'id_konsultasis':id_konsultasis, 'id_transaksis':id_transaksis, 'id_tindakan_polis':id_tindakan_polis, 'err':'terdapat error ketika ingin membuat tindakan, mohon dicoba lagi'})
    else:
        if(role == "admin"):
            return render(request, "buat_tindakan.html", {'id_konsultasis':id_konsultasis, 'id_transaksis':id_transaksis, 'id_tindakan_polis':id_tindakan_polis})
        else:
            return redirect('greenFeature:profile_page')
        
def daftarTindakanPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        daftar_tindakan_poli = []
        cursor.execute('SELECT * FROM tindakan')
        tindakans = cursor.fetchall()
        for tindakan in tindakans:
            cursor.execute('SELECT id_tindakan_poli FROM daftar_tindakan WHERE id_konsultasi = %s AND no_urut = %s', [tindakan[0], tindakan[1]])
            id_tindakan_polis = cursor.fetchall()
            tindakan_poli = [id_tindakan_poli[0] for id_tindakan_poli in id_tindakan_polis]
            daftar_tindakan_poli.append(tindakan_poli)
    mylist = zip(daftar_tindakan_poli, tindakans)
    if(role == "admin"):
        return render(request, "daftar_tindakan.html", {'mylist':mylist})
    else:
        return render(request, "daftar_tindakan_non_admin.html", {'mylist':mylist})
        
def updateTindakanPage(request, id_konsultasi, no_urut):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM tindakan WHERE id_konsultasi = %s AND no_urut = %s',[id_konsultasi, no_urut])
        tindakan = cursor.fetchone()
        cursor.execute('SELECT id_tindakan_poli FROM daftar_tindakan WHERE id_konsultasi = %s AND no_urut = %s', [id_konsultasi, no_urut])
        id_tindakan_polis = cursor.fetchall()
    if request.method == 'POST':
        catatan = request.POST["catatan"]
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE tindakan SET catatan = %s WHERE id_konsultasi = %s AND no_urut = %s', [catatan, id_konsultasi, no_urut])
            return redirect('greenFeature:daftar_tindakan')
        except DatabaseError as e:
            return render(request, "update_tindakan.html", {'tindakan':tindakan, 'id_tindakan_polis':id_tindakan_polis, 'err':'terdapat error ketika ingin update tindakan, mohon dicoba lagi, atau kembali ke daftar tindakan'})
    else:
        if(role == "admin" and tindakan):
            return render(request, "update_tindakan.html", {'tindakan':tindakan, 'id_tindakan_polis':id_tindakan_polis})
        else:
            return redirect('greenFeature:profile_page')

def deleteTindakanPage(request, id_konsultasi, no_urut):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    if(role == "admin"):
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('DELETE FROM tindakan WHERE id_konsultasi = %s AND no_urut = %s',[id_konsultasi, no_urut])
        except DatabaseError as e:
            return redirect('greenFeature:daftar_tindakan')
    return redirect('greenFeature:daftar_tindakan')
            
def buatTindakanPoliklinikPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_poliklinik FROM layanan_poliklinik ORDER BY RIGHT(id_poliklinik, -2)::int')
        id_polikliniks = cursor.fetchall()
    if request.method == 'POST':
        id_poliklinik = request.POST["id_poliklinik"]
        nama_tindakan = request.POST["nama_tindakan"]
        deskripsi = request.POST["deskripsi"]
        tarif = request.POST["tarif"]
        pk = generateIdTindakanPoli()
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO tindakan_poli VALUES (%s, %s, %s, %s, %s)', [pk, id_poliklinik, nama_tindakan, int(tarif), deskripsi])
            return redirect('greenFeature:daftar_tindakan_poliklinik')
        except DatabaseError as e:
            return render(request, "buat_tindakan_poliklinik.html", {'id_polikliniks':id_polikliniks, 'err':'terdapat error ketika ingin membuat tindakan poliklinik, mohon dicoba lagi'})
    else:
        if(role == "admin"):
            return render(request, "buat_tindakan_poliklinik.html", {'id_polikliniks':id_polikliniks})
        else:
            return redirect('greenFeature:profile_page')

def daftarTindakanPoliklinikPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM tindakan_poli ORDER BY RIGHT(id_tindakan_poli, -2)::int')
        tindakan_polis = cursor.fetchall()
    if(role == "admin"):
        return render(request, "daftar_tindakan_poliklinik.html", {'tindakan_polis':tindakan_polis})
    else:
        return render(request, "daftar_tindakan_poliklinik_non_admin.html", {'tindakan_polis':tindakan_polis})

def updateTindakanPoliklinikPage(request, id_tindakan_poli):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM tindakan_poli WHERE id_tindakan_poli = %s',[id_tindakan_poli])
        tindakan_poli = cursor.fetchone()
        cursor.execute('SELECT id_poliklinik FROM layanan_poliklinik ORDER BY RIGHT(id_poliklinik, -2)::int')
        id_polikliniks = cursor.fetchall()
    if request.method == 'POST':
        id_poliklinik = request.POST["id_poliklinik"]
        nama_tindakan = request.POST["nama_tindakan"]
        deskripsi = request.POST["deskripsi"]
        tarif = request.POST["tarif"]
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE tindakan_poli SET id_poliklinik = %s, nama_tindakan = %s, tarif = %s, deskripsi = %s WHERE id_tindakan_poli = %s', [id_poliklinik, nama_tindakan, int(tarif), deskripsi, id_tindakan_poli])
            return redirect('greenFeature:daftar_tindakan_poliklinik')
        except DatabaseError as e:
            return render(request, "update_tindakan_poliklinik.html", {'tindakan_poli':tindakan_poli, 'id_polikliniks':id_polikliniks, 'err':'terdapat error ketika ingin update tindakan poliklinik, mohon dicoba lagi atau kembali ke daftar tindakan poliklinik'})
    else:
        if(role == "admin" and tindakan_poli):
            return render(request, "update_tindakan_poliklinik.html", {'tindakan_poli':tindakan_poli, 'id_polikliniks':id_polikliniks})
        return redirect('greenFeature:profile_page')
  
def deleteTindakanPoliklinikPage(request, id_tindakan_poli):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    if(role == "admin"):
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('DELETE FROM tindakan_poli WHERE id_tindakan_poli = %s',[id_tindakan_poli])
        except DatabaseError as e:
            redirect('greenFeature:daftar_tindakan_poliklinik')
    return redirect('greenFeature:daftar_tindakan_poliklinik')
    
def getUserBasedOnRole(role, username):
    with connection.cursor() as cursor:
        if role == "admin":
            cursor.execute('SELECT * FROM administrator WHERE username = %s',[username])
            admin = cursor.fetchone()
            return admin
        elif role == "dokter":
            cursor.execute('SELECT * FROM dokter WHERE username = %s',[username])
            dokter = cursor.fetchone()
            return dokter
        else:
            cursor.execute('SELECT * FROM pasien WHERE username = %s',[username])
            pasien = cursor.fetchone()
            return pasien
            
def generateIdTindakanPoli():
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_tindakan_poli FROM tindakan_poli ORDER BY RIGHT(id_tindakan_poli, -2)::int DESC LIMIT 1')
        tindakan_poli = cursor.fetchone()
        if(tindakan_poli):
            id_dokter = tindakan_poli[0]
            new_number = int(id_dokter[2::])+1
            return "TP"+str(new_number)
        return "TP1"
        
def generateNomorUrut(id_konsultasi):
    with connection.cursor() as cursor:
        cursor.execute("SELECT no_urut FROM tindakan WHERE id_konsultasi = %s ORDER BY no_urut::int DESC LIMIT 1",[id_konsultasi])
        no_urut = cursor.fetchone()
        if(no_urut):
            new_number = int(no_urut[0::])+1
            return new_number
        return 1