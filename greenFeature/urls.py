from django.urls import path
from . import views

app_name = 'greenFeature'
urlpatterns = [
    path('profile/', views.profilePage, name='profile_page'),
    
    path('buat-tindakan/', views.buatTindakanPage, name='buat_tindakan'),
    
    path('daftar-tindakan/', views.daftarTindakanPage, name='daftar_tindakan'),
    
    path('update-tindakan/<id_konsultasi>/<no_urut>', views.updateTindakanPage, name='update_tindakan'),
    path('update-tindakan/', views.updateTindakanPage, name='update_tindakan'),
    
    path('buat-tindakan-poliklinik/', views.buatTindakanPoliklinikPage, name='buat_tindakan_poliklinik'),
    
    path('daftar-tindakan-poliklinik/', views.daftarTindakanPoliklinikPage, name='daftar_tindakan_poliklinik'),
    
    path('update-tindakan-poliklinik/<id_tindakan_poli>', views.updateTindakanPoliklinikPage, name='update_tindakan_poliklinik'),
    path('update-tindakan-poliklinik/', views.updateTindakanPoliklinikPage, name='update_tindakan_poliklinik'),
    
    path('delete-tindakan-poliklinik/<id_tindakan_poli>', views.deleteTindakanPoliklinikPage, name='delete_tindakan_poliklinik'),
    path('delete-tindakan-poliklinik/', views.deleteTindakanPoliklinikPage, name='delete_tindakan_poliklinik'),
    
    path('delete-tindakan/<id_konsultasi>/<no_urut>', views.deleteTindakanPage, name='delete_tindakan'),
    path('delete-tindakan/', views.deleteTindakanPage, name='delete_tindakan'),
]