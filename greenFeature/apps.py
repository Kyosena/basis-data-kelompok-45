from django.apps import AppConfig


class GreenfeatureConfig(AppConfig):
    name = 'greenFeature'
