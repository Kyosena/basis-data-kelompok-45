from django.urls import path
from . import views

app_name = 'sesikonsultasi'

urlpatterns = [
    path('buat-sesikonsultasi/', views.buatSesiPage, name='buat_sesikonsultasi'),

    path('daftar-sesikonsultasi/', views.daftarSesiPage, name='daftar_sesikonsultasi'),

    path('update-sesikonsultasi/<id_konsultasi>/<no_rekam_medis>/', views.updateSesiPage, name='update_sesikonsultasi'),
    path('update-sesikonsultasi/', views.updateSesiPage, name='update_sesikonsultasi'),

    path('buat-rscabang/', views.buatRscabangPage, name='buat_rscabang'),

    path('daftar-rscabang/', views.daftarRscabangPage, name='daftar_rscabang'),
    
    path('update-rscabang/<kode_rs>', views.updateRscabangPage, name='update_rscabang'),
    path('update-rscabang/', views.updateRscabangPage, name='update_rscabang'),

    path('delete-sesikonsultasi/<id_konsultasi>/<no_rekam_medis>', views.deleteSesiPage, name='delete_sesikonsultasi'),
    path('delete-sesikonsultasi/', views.deleteSesiPage, name='delete_sesikonsultasi'),

    path('delete-rscabang/<kode_rs>', views.deleteRscabangPage, name='delete_rscabang'),
    path('delete-rscabang/', views.deleteRscabangPage, name='delete_rscabang'),
    # dilanjutkan ...
]