from django.db import connection, DatabaseError, transaction
from django.shortcuts import render, redirect

def buatSesiPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT no_rekam_medis FROM pasien')
        no_rekam_medis_all = cursor.fetchall()
        cursor.execute('SELECT id_transaksi FROM transaksi')
        id_transaksi_all = cursor.fetchall()
    if request.method == 'POST':
        no_rekam_medis = request.POST["no_rekam_medis"]
        tanggal = request.POST["tanggal"]
        id_transaksi = request.POST["id_transaksi"]
        id_konsultasi = generateIdKonsultasi()
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                        cursor.execute('INSERT INTO sesi_konsultasi VALUES (%s, %s, %s, %s, %s, %s)', [id_konsultasi, no_rekam_medis, tanggal, 0, "booked", id_transaksi])
            return redirect('redFeature:daftar_sesikonsultasi')
        except DatabaseError as e:
            print("error")
            return render(request, "buatsesikonsultasi_admin.html", {'id_transaksi_all':id_transaksi_all, 'no_rekam_medis_all':no_rekam_medis_all})
    else:
        if (role == "admin"):
            return render(request, 'buatsesikonsultasi_admin.html', {'id_transaksi_all':id_transaksi_all, 'no_rekam_medis_all':no_rekam_medis_all})
        else:
            return redirect('greenFeature:profile_page')


def buatRscabangPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    if request.method == 'POST':
        kode_rs = request.POST["kode_rs"]
        nama = request.POST["nama"]
        tanggal = request.POST["tanggal"]
        jalan = request.POST["jalan"]
        nomor = request.POST["nomor"]
        kota = request.POST["kota"]
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO rs_cabang VALUES (%s, %s, %s, %s, %s, %s)', [kode_rs, nama, tanggal, jalan, nomor, kota])
            return redirect('redFeature:daftar_rscabang')        
        except DatabaseError as e:
            return render(request, 'buatrscabang.html')
    else:
        if(role == "admin"):
            return render(request, 'buatrscabang.html')
        else:
            return redirect('greenFeature:profile_page')
        


def daftarSesiPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        daftar_sesi_konsultasi = []
        cursor.execute('SELECT * FROM sesi_konsultasi')
        sesi_konsultasi_all = cursor.fetchall()
        for sesi_konsultasi in sesi_konsultasi_all:
            cursor.execute('SELECT id_konsultasi FROM sesi_konsultasi')
            id_konsultasi_all = cursor.fetchall()
            sesi = [id_konsultasi[0] for id_konsultasi in id_konsultasi_all]
            daftar_sesi_konsultasi.append(sesi)
    mylist = zip(daftar_sesi_konsultasi, sesi_konsultasi_all)
    if (role == "admin"):
        return render(request, 'daftarsesikonsultasi_admin.html', {'mylist':mylist})
    else:
        return render(request, 'daftarsesikonsultasi_nonadmin.html', {'mylist':mylist})


def daftarRscabangPage(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        daftar_rs_cabang = []
        cursor.execute('SELECT * FROM rs_cabang')
        rs_cabang_all = cursor.fetchall()
        for rs_cabang in rs_cabang_all:
            cursor.execute('SELECT kode_rs FROM rs_cabang')
            kode_rs_all = cursor.fetchall()
            rs = [kode_rs[0] for kode_rs in kode_rs_all]
            daftar_rs_cabang.append(rs)
    mylist = zip(daftar_rs_cabang, rs_cabang_all)
    if(role == "admin"):
        return render(request, 'daftarrscabang_admin.html', {'mylist':mylist})
    else:
        return render(request, 'daftarrscabang_nonadmin.html', {'mylist':mylist})



def updateSesiPage(request, id_konsultasi, no_rekam_medis):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM sesi_konsultasi WHERE id_konsultasi = %s AND no_rekam_medis_pasien = %s',[id_konsultasi, no_rekam_medis])
        sesi_konsultasi = cursor.fetchone()
        cursor.execute('SELECT id_konsultasi FROM sesi_konsultasi WHERE id_konsultasi = %s AND no_rekam_medis_pasien = %s',[id_konsultasi, no_rekam_medis])
        id_konsultasi_all = cursor.fetchall()
    if request.method == 'POST':
        tanggal = request.POST["tanggal"]
        status = request.POST["status"]
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    print("masok")
                    print(status)
                    cursor.execute('UPDATE sesi_konsultasi SET tanggal = %s, status = %s WHERE id_konsultasi = %s' , [tanggal, status, id_konsultasi])
            return redirect('redFeature:daftar_sesikonsultasi')
        except DatabaseError as e:
            print("error")
            return render(request, 'updatesesikonsultasi.html', {'sesi_konsultasi':sesi_konsultasi, 'id_konsultasi_all':id_konsultasi_all})    
    else:
        if(role == "admin" and sesi_konsultasi):
            return render(request, 'updatesesikonsultasi.html', {'sesi_konsultasi':sesi_konsultasi, 'id_konsultasi_all':id_konsultasi_all})    
        else:
            return redirect('greenFeature:profile_page')   

def updateRscabangPage(request, kode_rs):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM rs_cabang WHERE kode_rs = %s',[kode_rs])
        rs_cabang = cursor.fetchone()
    if request.method == 'POST':
        nama = request.POST["nama"]
        tanggal = request.POST["tanggal"]
        jalan = request.POST["jalan"]
        kota = request.POST["kota"]
        nomor = request.POST["nomor"]
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE rs_cabang SET nama = %s, tanggal_pendirian = %s, jalan = %s, kota= %s, nomor= %s', [nama, tanggal, jalan, kota, nomor])
            return redirect('redFeature:daftar_rscabang')
        except DatabaseError as e:
            return render(request, 'updaterscabang.html', {'rs_cabang': rs_cabang})
    else:
        if(role == "admin" and rs_cabang):
            return render(request, 'updaterscabang.html', {'rs_cabang': rs_cabang})
        else:
            return redirect('greenFeature:profile_page')

def deleteSesiPage(request, id_konsultasi, no_rekam_medis):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    if(role == "admin"):
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('DELETE FROM sesi_konsultasi WHERE id_konsultasi = %s AND no_rekam_medis_pasien = %s',[id_konsultasi, no_rekam_medis])
        except DatabaseError as e:
             return redirect('redFeature:daftar_sesikonsultasi')
    return redirect('redFeature:daftar_sesikonsultasi')

def deleteRscabangPage(request, kode_rs):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    if(role == "admin"):
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('DELETE FROM rs_cabang WHERE kode_rs = %s',[kode_rs])
        except DatabaseError as e:
             return redirect('redFeature:daftar_rscabang')
    return redirect('redFeature:daftar_rscabang')

def generateIdKonsultasi():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_konsultasi FROM sesi_konsultasi ORDER BY RIGHT(id_konsultasi, -2)::int DESC")
        sesi_konsultasi = cursor.fetchone()
        print(sesi_konsultasi)
        if(sesi_konsultasi):
            id_dokter = sesi_konsultasi[0]
            print(id_dokter)
            new_number = int(id_dokter[2::])+1
            print(new_number)
            return "SK"+str(new_number)
        return "SK1"
            