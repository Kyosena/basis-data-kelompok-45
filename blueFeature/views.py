from django.db import connection, DatabaseError, transaction
from django.shortcuts import render, redirect, reverse
import datetime

def buatTransaksi(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
        
    with connection.cursor() as cursor:
        cursor.execute('SELECT no_rekam_medis FROM pasien')
        no_rekam_mediss = cursor.fetchall()

    if request.method == 'POST':
        id_transaksi = generateIdTransaksi()
        tanggal = datetime.date.today()
        status = 'Created'
        waktu_pembayaran = datetime.datetime.now()
        waktu_pembayaran_full = waktu_pembayaran.strftime("%Y-%m-%d %H:%M:%S")
        no_rekam_medis = request.POST["no_rekam_medis"]
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO transaksi VALUES (%s, %s, %s, %s, %s, %s)', [id_transaksi, tanggal, status, 0, waktu_pembayaran_full, no_rekam_medis])
            return redirect('blueFeature:RUDadmin')
        except DatabaseError as e:
            return render(request, "buat_transaksi.html", {'no_rekam_mediss':no_rekam_mediss})
    else:
        if(role == "admin"):
            return render(request, "buat_transaksi.html", {'no_rekam_mediss':no_rekam_mediss})
        else:
            return redirect('blueFeature:buat_transaksi')

def rudAdmin(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
        
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM transaksi')
        transaksis = cursor.fetchall()
    if(role == "admin"):
        return render(request, "RUDadmin.html", {'transaksis':transaksis})
    else:
        return render(request, "RUDpasiendokter.html", {'transaksis':transaksis})


def updateTransaksi(request, id_transaksi):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
        
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM transaksi WHERE id_transaksi = %s',[id_transaksi])
        transaksis = cursor.fetchone()
    if request.method == 'POST':
        id_transa = id_transaksi
        tanggal = request.POST["tanggal"]
        status = request.POST["status"]
        waktu_pembayaran = request.POST["waktu_pembayaran"]
        try:
            with transaction.atomic():
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE transaksi SET tanggal = %s, status = %s, waktu_pembayaran = %s WHERE id_transaksi = %s', [tanggal, status, waktu_pembayaran, id_transa])
            return redirect('blueFeature:RUDadmin')
        except DatabaseError as e:
            return render(request, "update_transaksi.html", {'transaksis':transaksis})
    else:
        if(role == "admin"):
            return render(request, "update_transaksi.html", {'transaksis':transaksis})
        return redirect('greenFeature:profile_page')

def deleteTransaksi(request, id_transaksi):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
        
    if(role == "admin"):
        with transaction.atomic():
            with connection.cursor() as cursor:           
                cursor.execute('DELETE FROM transaksi WHERE id_transaksi = %s',[id_transaksi])
                print(id_transaksi)
    return redirect('blueFeature:RUDadmin')

def generateIdTransaksi():
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_transaksi FROM transaksi ORDER BY RIGHT(id_transaksi, -2)::int DESC LIMIT 1')
        transaksi = cursor.fetchone()
        if(transaksi):
            id_transaksi = transaksi[0]
            new_number = int(id_transaksi[2::])+1
            return "TK"+str(new_number)
        return "TK1"
# Create your views here.
