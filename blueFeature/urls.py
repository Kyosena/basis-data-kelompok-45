from django.urls import path
from . import views

app_name = 'blueFeature'

urlpatterns = [
    path('buatTransaksi/', views.buatTransaksi, name='buatTransaksi'),
    
    path('RUDadmin/', views.rudAdmin, name='RUDadmin'),
    
    path('updateTransaksi/<id_transaksi>', views.updateTransaksi, name='updateTransaksi'),
    path('updateTransaksi/', views.updateTransaksi, name='updateTransaksi'),

    path('deleteTransaksi/<id_transaksi>', views.deleteTransaksi, name='deleteTransaksi'),
    path('deleteTransaksi/', views.deleteTransaksi, name='deleteTransaksi'),
]