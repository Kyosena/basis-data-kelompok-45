from django.urls import path
from . import views

app_name = "purpleFeature"

urlpatterns = [
    path('daftardokter/',views.daftardokter,name = "daftardokter"),

    path('jadwal-poliklinik/',views.jadwalpoliklinik,name="jadwalpoliklinik"),

    path('jadwal-poliklinik-details/<id_poliklinik>',views.jadwalpoliklinikdetails,name = "jadwalpoliklinikdetails"),
    path('jadwal-poliklinik-details/',views.jadwalpoliklinikdetails,name = "jadwalpoliklinikdetails"),

    path('pembuatan-layanan-poliklinik/',views.buatlayananpoliklinik,name = "buatlayananpoliklinik"),

    path('update-daftar-dokter/<id_dokter>/<kode_rs>',views.updatedaftardokter,name = "updatedaftardokter"),
    path('update-daftar-dokter/',views.updatedaftardokter,name="updatedaftardokter"),

    path('daftar-dokter-rs-cabang/',views.daftardoktercabang,name = "daftardoktercabang"),

    path('update-layanan-poliklinik/<id_poliklinik>/<kode_rs>',views.updatelayananpoliklinik,name = "updatelayananpoliklinik"),
    path('update-layanan-poliklinik/',views.updatelayananpoliklinik,name = "updatelayananpoliklinik"),

    path('update-jadwal-poliklinik/<id_jadwal_layanan_poliklinik>',views.updatejadwalpoliklinik,name = "updatejadwalpoliklinik"),
    path('update-jadwal-poliklinik/',views.updatejadwalpoliklinik,name = "updatejadwalpoliklinik"),

    path('delete-layanan-poliklinik/<id_poliklinik>/<kode_rs>',views.deletelayananpoliklinik,name="deletelayananpoliklinik"),
    path('delete-layanan-poliklinik',views.deletelayananpoliklinik,name="deletelayananpoliklinik"),

    path('delete-jadwal-poliklinik/<id_poliklinik>/<id_layanan>',views.deletejadwalpoliklinik,name="deletejadwalpoliklinik"),
    path('delete-jadwal-poliklinik/',views.deletejadwalpoliklinik,name="deletejadwalpoliklinik"),

    path('delete-dokter/<id_dokter>/<kode_rs>',views.deletedokter,name="deletedokter"),
    path('delete-dokter/',views.deletedokter,name="deletedokter")
]