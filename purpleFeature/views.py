from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection, DatabaseError, transaction
from django.views.generic import CreateView
from .views import *
from firstFeature.views import *

# Create your views here.
def daftardokter(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()
    cursor.execute("SELECT RS.kode_rs, ARRAY_AGG(D.id_dokter) as doctors FROM dokter_rs_cabang as RS LEFT JOIN dokter as D ON D.id_dokter = RS.id_dokter WHERE D.id_dokter = RS.id_dokter GROUP BY RS.kode_rs ORDER BY RS.kode_rs;"),
    arguements = {
        'doctors': cursor.fetchall()
    }
    if(role == 'admin'):
        return render(request,"DaftarDokterAdmin.html",arguements)
    else:
        return render(request,"DaftarDokter.html",arguements)


def jadwalpoliklinik(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()
    
    cursor.execute("SELECT * from layanan_poliklinik;")
    objects = cursor.fetchall()
    args = {
        'services':objects
    }
    if(role == 'admin'):
        return render(request,"JadwalPoliklinikAdmin.html",args)
    else:
        return render(request,"JadwalPoliklinik.html",args)


def jadwalpoliklinikdetails(request,id_poliklinik):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()
    
    cursor.execute("SELECT * from jadwal_layanan_poliklinik WHERE id_poliklinik=%s;",[id_poliklinik])
    objects = cursor.fetchall()
    args = {
        'schedules':objects
    }
    if(role == 'admin'):
        return render(request,"JadwalPoliklinikdetailsAdmin.html",args)
    else:
        return render(request,"JadwalPoliklinikdetails.html",args)


def buatlayananpoliklinik(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM rs_cabang ORDER BY kode_rs")
    rs = cursor.fetchall()
    cursor.execute("SELECT id_dokter FROM dokter")
    doctors=cursor.fetchall()

    if request.method == 'POST':
        cursor.execute("SELECT MAX(LENGTH(id_poliklinik)) FROM layanan_poliklinik;")
        length1=cursor.fetchone()
        cursor.execute("SELECT MAX(id_poliklinik) FROM layanan_poliklinik WHERE LENGTH(id_poliklinik)=%s",[length1])
        max = cursor.fetchone()[0][2:]
        print(max)
        newmax=str(int(max) + 1)
        newid='PL' + newmax
        nama=request.POST.get('nama')
        deskripsi=request.POST.get('deskripsi')
        kode_rs=request.POST.get('kode_rs')
        hari_list=request.POST.getlist('hari')
        waktu_mulai_list=request.POST.getlist('waktu_mulai')
        waktu_selesai_list=request.POST.getlist('waktu_selesai')
        kapasitas_list=request.POST.getlist('kapasitas')
        dokter_list=request.POST.getlist('dokter')
        with transaction.atomic():
            cursor.execute("INSERT INTO layanan_poliklinik(id_poliklinik,kode_rs_cabang,nama,deskripsi)VALUES(%s,%s,%s,%s);",[newid,kode_rs,nama,deskripsi])
        for i in range(len(hari_list)):
            with transaction.atomic():
                try:
                    cursor.execute("SELECT MAX(LENGTH(id_jadwal_poliklinik)) FROM jadwal_layanan_poliklinik")
                    length=cursor.fetchone()
                    cursor.execute("SELECT MAX(id_jadwal_poliklinik) FROM (SELECT id_jadwal_poliklinik FROM jadwal_layanan_poliklinik WHERE LENGTH(id_jadwal_poliklinik)=%s) as length;",[length])
                    maximum=cursor.fetchone()[0][3:]
                    print('maximum = '  + maximum)
                    newmaximum=str(int(maximum) + 1)
                    print(newmaximum)
                    newjpl='JPL'+newmaximum
                    cursor.execute("INSERT INTO jadwal_layanan_poliklinik(id_jadwal_poliklinik,waktu_mulai,waktu_selesai,hari,kapasitas,id_dokter,id_poliklinik) VALUES(%s,%s,%s,%s,%s,%s,%s)",[newjpl,waktu_mulai_list[i],waktu_selesai_list[i],hari_list[i],kapasitas_list[i],dokter_list[i],newid])
                    print(connection.queries)
                except DatabaseError as Error:
                    print(Error)
        return redirect("purpleFeature:jadwalpoliklinik")
    if(role == 'admin'):
        return render(request,"FormPembuatanLayananPoliklinik.html",{'rs':rs,'doctors':doctors})
    else:
        return redirect('greenFeature:profile_page')

def daftardoktercabang(request):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    if(role == 'admin'):
        cursor = connection.cursor()
        cursor.execute("SELECT id_dokter FROM dokter")
        doctors=cursor.fetchall()
        cursor.execute("SELECT kode_rs FROM rs_cabang")
        rs=cursor.fetchall()
        if request.method == 'POST':
            id_dokter = request.POST['id_dokter']
            kode_rs = request.POST['kode_rs']
            with transaction.atomic():
                try:
                    cursor.execute("INSERT INTO dokter_rs_cabang(id_dokter,kode_rs) VALUES(%s,%s)",[id_dokter,kode_rs])
                    return redirect("purpleFeature:daftardokter")
                    print(connection.queries)
                except DatabaseError as error:
                    print(error)
                    return redirect("purpleFeature:daftardoktercabang")
        args={
            'doctors':doctors,
            'rs':rs
        }
        return render(request,"FormDaftarDokterRsCabang.html",args)
    else:
        return redirect('greenFeature:profile_page')



def updatedaftardokter(request,id_dokter,kode_rs):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    if(role == 'admin'):
        cursor=connection.cursor()
        if request.method == 'POST':
            with transaction.atomic():
                try:
                    id_dokter1 = request.POST['id_dokter']
                    kode_rs1 = request.POST['kode_rs']
                    cursor.execute("UPDATE dokter_rs_cabang SET id_dokter=%s,kode_rs=%s WHERE id_dokter=%s AND kode_rs=%s",[id_dokter1,kode_rs1,id_dokter,kode_rs])
                    print(connection.queries)
                    return redirect("purpleFeature:daftardokter")
                except DatabaseError as error:
                    print(error)
                    return redirect("purpleFeature:daftardokter")
        cursor.execute("SELECT id_dokter,kode_rs FROM dokter_rs_cabang WHERE id_dokter=%s AND kode_rs=%s",[id_dokter,kode_rs])
        arg=cursor.fetchone()
        cursor.execute("SELECT id_dokter FROM dokter_rs_cabang WHERE id_dokter NOT LIKE %s",[id_dokter])
        arg2=cursor.fetchall()
        cursor.execute("SELECT kode_rs FROM dokter_rs_cabang WHERE kode_rs NOT IN(SELECT kode_rs FROM dokter_rs_cabang WHERE id_dokter=%s )",[id_dokter])
        arg3=cursor.fetchall()
        arguements={
            'current':arg,
            'alldoctors':arg2,
            'allcodes':arg3
        }
        return render(request,"UpdateDaftarDokter.html",arguements)
    else:
        return redirect('greenFeature:profile_page')


def updatelayananpoliklinik(request,id_poliklinik,kode_rs):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()
    if request.method == 'POST':
        id_poliklinik1 = request.POST.get('id_poliklinik')
        print(id_poliklinik)
        nama = request.POST['nama_layanan']
        kode_rs_cabang1 = request.POST.get('kode_rs')
        deskripsi = request.POST['deskripsi']
        with transaction.atomic():
            try:
                cursor.execute("UPDATE layanan_poliklinik SET nama=%s,deskripsi=%s WHERE id_poliklinik=%s AND kode_rs_cabang=%s;",[nama,deskripsi,id_poliklinik,kode_rs])
                print(connection.queries)
                return redirect('purpleFeature:jadwalpoliklinik')
            except DatabaseError as Error:
                print(error)
    cursor.execute("SELECT * FROM layanan_poliklinik WHERE id_poliklinik=%s;",[id_poliklinik])
    poly=cursor.fetchone()

    args={
        'poliklinik':poly
    }

    if(role == 'admin'):
        return render(request,"FormUpdateLayananPoliklinik.html",args)
    else:
        return redirect('greenFeature:profile_page')


def deletelayananpoliklinik(request,id_poliklinik,kode_rs):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()

    if(role == 'admin'):
        with transaction.atomic():
            try:
                cursor.execute("DELETE FROM layanan_poliklinik WHERE id_poliklinik=%s AND kode_rs_cabang=%s",[id_poliklinik,kode_rs])
                return redirect("purpleFeature:jadwalpoliklinik")
            except DatabaseError as Error:
                print(Error)


def deletejadwalpoliklinik(request,id_layanan):
    role=getPenggunaType(username)

    return render(request,"DaftarDokter.html")

def updatejadwalpoliklinik(request,id_jadwal_layanan_poliklinik):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM jadwal_layanan_poliklinik WHERE id_jadwal_poliklinik=%s",[id_jadwal_layanan_poliklinik])
    schedule = cursor.fetchone()
    if request.method == 'POST':
        waktu_mulai=request.POST['waktu_mulai']
        waktu_selesai=request.POST['waktu_selesai']
        kapasitas=request.POST['kapasitas']
        hari=request.POST['hari']
        id_dokter=request.POST['id_dokter']
        id_poliklinik=request.POST.get('id_poliklinik','')
        with transaction.atomic():
            try:
                cursor.execute("UPDATE jadwal_layanan_poliklinik SET waktu_mulai=%s,waktu_selesai=%s,kapasitas=%s,hari=%s,id_dokter=%s WHERE id_jadwal_poliklinik=%s",[waktu_mulai,waktu_selesai,kapasitas,hari,id_dokter,id_jadwal_layanan_poliklinik])
                print(connection.queries)
                return redirect("purpleFeature:jadwalpoliklinikdetails",id_poliklinik=schedule[6])
            except DatabaseError as Error:
                print(Error)
    cursor.execute("SELECT id_dokter FROM dokter WHERE id_dokter NOT LIKE %s",[schedule[5]])
    doctors=cursor.fetchall()

    args={
        'schedule':schedule,
        'doctors':doctors
    }
    if(role == 'admin'):
        return render(request,"FormUpdateJadwalLayananPoliklinik.html",args)
    else:
        return redirect('greenFeature:profile_page')


def deletejadwalpoliklinik(request,id_poliklinik,id_layanan):
    if 'Username' not in request.session:
        return redirect("firstFeature:login_page")
    else:
        role = request.session['Role']
        username = request.session['Username']
    cursor = connection.cursor()
    if(role == 'admin'):
        with transaction.atomic():
            try:
                cursor.execute("DELETE FROM jadwal_layanan_poliklinik WHERE id_jadwal_poliklinik=%s",[id_layanan])
                return redirect("purpleFeature:jadwalpoliklinikdetails",id_poliklinik=id_poliklinik)
            except DatabaseError as Error:
                print(error)




def deletedokter(request,id_dokter,kode_rs):
    cursor=connection.cursor()
    try:
        with transaction.atomic():
            with connection.cursor() as cursor:
                    cursor.execute("DELETE FROM dokter_rs_cabang WHERE id_dokter=%s AND kode_rs=%s",[id_dokter,kode_rs])
                    print(connection.queries)
    except DatabaseError as error:
        print(error)
        return redirect('purpleFeature:daftardokter')
    return redirect('purpleFeature:daftardokter')
