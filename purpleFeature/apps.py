from django.apps import AppConfig


class PurplefeatureConfig(AppConfig):
    name = 'purpleFeature'
