from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve,reverse
from .views import *

class purpleFeatureTest(TestCase):
#URLS and VIEWS
    def test_url_isexist(self):
        response = Client().get(reverse('purpleFeature:daftardokter',args = ['Admin','Kyosena']))
        self.assertEquals(response.status_code,200)
        response = Client().get(reverse('purpleFeature:daftardokter',args = ['pasien','sickyo']))
        self.assertEquals(response.status_code,200)
        response = Client().get(reverse('purpleFeature:jadwalpoliklinik',args = ['Admin','Kyosena']))
        self.assertEquals(response.status_code,200)
        response = Client().get(reverse('purpleFeature:jadwalpoliklinik',args = ['pasien','sickyo']))
        self.assertEquals(response.status_code,200)
        response = Client().get(reverse('purpleFeature:jadwalpoliklinikdetails',args = ['Admin','Kyosena']))
        self.assertEquals(response.status_code,200)
        response = Client().get(reverse('purpleFeature:jadwalpoliklinikdetails',args = ['pasien','sickyo']))
        self.assertEquals(response.status_code,200)
        response = Client().get(reverse('purpleFeature:buatlayananpoliklinik' ,args = ['Admin','Kyosena']))
        self.assertEquals(response.status_code,302)
        response = Client().get(reverse('purpleFeature:buatlayananpoliklinik', args = ['pasien','sickyo']))
        self.assertEquals(response.status_code,302)
    def test_sign_url_resolve(self):
        url = reverse('purpleFeature:daftardokter',args = ['Admin','Kyosena'])
        self.assertEquals(resolve(url).func,daftardokter) 
        url = reverse('purpleFeature:daftardokter',args = ['pasien','sickyo'])
        self.assertEquals(resolve(url).func,daftardokter) 
        url = reverse('purpleFeature:jadwalpoliklinik',args = ['Admin','Kyosena'])
        self.assertEquals(resolve(url).func,jadwalpoliklinik)
        url = reverse('purpleFeature:jadwalpoliklinik',args = ['pasien','sickyo'])
        self.assertEquals(resolve(url).func,jadwalpoliklinik)
        url = reverse('purpleFeature:jadwalpoliklinikdetails',args = ['Admin','Kyosena'])
        self.assertEquals(resolve(url).func,jadwalpoliklinikdetails)
        url = reverse('purpleFeature:jadwalpoliklinikdetails',args = ['pasien','sickyo'])
        self.assertEquals(resolve(url).func,jadwalpoliklinikdetails)
        url = reverse('purpleFeature:buatlayananpoliklinik',args = ['Admin','Kyosena'])
        self.assertEquals(resolve(url).func,buatlayananpoliklinik)
        url = reverse('purpleFeature:buatlayananpoliklinik',args = ['pasien','sickyo'])
        self.assertEquals(resolve(url).func,buatlayananpoliklinik)

